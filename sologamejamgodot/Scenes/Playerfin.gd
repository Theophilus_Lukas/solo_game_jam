extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -613
export (int) var jumpcount = 0
export (int) var maxjumpcount = 1

const UP = Vector2(0,-1)

var velocity = Vector2()

func get_input():
	velocity.x = 0
	var animation = "diam_kanan"
	
	#if is_on_floor() and Input.is_action_just_pressed('up'):
	#	velocity.y = jump_speed
	if Input.is_action_pressed('up') and jumpcount < maxjumpcount:
		animation = "teabag"
		
	if Input.is_action_just_pressed('down') and jumpcount < maxjumpcount:
		$jump.play()
		jumpcount+=1
		velocity.y = -jump_speed
		animation = "loncat_kanan"
		jump_speed = -613
		
	if is_on_ceiling():
		jumpcount = 0
		
	if !is_on_ceiling():
		if Input.is_action_pressed('right'):
			$AnimatedSprite.flip_h = false
			velocity.x += speed
			animation = "loncat_kanan"
		elif Input.is_action_pressed('left'):
			velocity.x -= speed
			animation = "loncat_kanan"
			$AnimatedSprite.flip_h = true
		else:
			animation = "loncat_neut"
			
	if Input.is_action_pressed('emote') and is_on_ceiling():
		if jump_speed > -750: 
			jump_speed -= 10
			animation = 'powerup1'
		elif jump_speed > -900:
			jump_speed -= 10
			animation = 'powerup2'
		elif jump_speed > -1050:
			jump_speed -= 10
			animation = 'powerup3'
		else:
			animation = 'powerup3'
			
	if Input.is_action_pressed('right') and is_on_ceiling():
		$AnimatedSprite.flip_h = false
		velocity.x += speed
		animation = "jalan_kanan"
		
	if Input.is_action_pressed('left') and is_on_ceiling():
		velocity.x -= speed
		animation = "jalan_kanan"
		$AnimatedSprite.flip_h = true
		
	if $AnimatedSprite.animation != animation:
		$AnimatedSprite.play(animation)

	
func _physics_process(delta):
	velocity.y -= delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)


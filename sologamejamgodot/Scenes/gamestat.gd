extends Node

onready var game_start_time = OS.get_ticks_msec()
func get_time():
	var current_time = OS.get_ticks_msec() - game_start_time
	var minutes = current_time/1000/60
	var second = current_time/1000%60
	var msec = current_time%1000
	
	if minutes < 10:
		minutes = "0"+str(minutes)
	if second < 10:
		second = "0"+str(second)
	if msec < 10:
		msec = "00"+str(msec)
	elif msec < 100:
		msec = "0"+str(msec)
	return (str(minutes)+":"+str(second)+":"+str(msec))
